export interface Notifications {
    Notification: Notification[],
  }
  
  export interface Notification {
    app_id: string,
    cust_id: string,
    not_datetime: string,
    not_id:string,
    
    not_is_read:string,
    not_message:string,
    sal_id:string,
    sal_name:string,
    sal_pic:string,

}

export interface pet {
    name: string;
    age: number;
    weight: number;
}
