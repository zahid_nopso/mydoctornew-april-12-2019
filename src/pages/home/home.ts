import { CustomerModal } from "./../../providers/CustomerModal/CustomerModal";
import { SalonServicesModal } from "./../../providers/salon-services-modal/salon-services-modal";
import { SalonServices, Salon, APP_CONFIG } from "./../../providers/SalonAppUser-Interface";
import { SQLiteObject, SQLite } from "@ionic-native/sqlite";
import { Keyboard } from "@ionic-native/keyboard";
import { Geolocation } from '@ionic-native/geolocation';
import {

  SALON_SERVICES_SEARCH,
  Customer,
  OFFERS,
  SalonHour
} from "./../../providers/SalonAppUser-Interface";
import { config } from "./../../providers/config/config";

import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ViewController,
  Platform,
  Nav,
  Events,
  MenuController,
  App,
  AlertController
} from "ionic-angular";
import { MainHomePage } from "../main-home/main-home";
import { timer } from "rxjs/observable/timer";
import { SalonDetailsPage } from "../salon-details/salon-details";
import { AllOffersPage } from "../all-offers/all-offers";
import { Storage } from "@ionic/storage";

import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { TabsPage } from "../tabs/tabs";
import { NgZone } from "@angular/core";
import { last } from "rxjs/operators";

import { GlobalProvider } from "../../providers/global/global";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { GoogleAnalytics } from "../../../node_modules/@ionic-native/google-analytics";
import { PopoverController } from "ionic-angular";
import { NOPSO_Header_OPTIONS } from '../../providers/SalonAppUser-Interface';
import { AppointmentsCardPage } from "../appointments-card/appointments-card";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
// import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { Content } from "ionic-angular";
import { LoginSignUpPage } from "../login-sign-up/login-sign-up";
import { ConfigModal } from "../../providers/ConfigModal/ConfigModal";
//http://pointdeveloper.com/hide-ionic-2-tab-bar-specific-tabs/
import * as $ from "jquery";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  public ALLOFFERS: OFFERS[];

  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;
  @ViewChild("scroll") scroll: any;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };
  public reRequestCounter = 0;
  public slideOutNow = false;
  public isFavourites = false;
  public infiniteScroll: any;
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS;
  public allOffers: OFFERS[];
  public offersForListing: OFFERS[];
  public subStyleImageName: string;
  public pinImage: string;
  public subStyleName: string;
  public txtSearchSalon: string;
  public serverSearchlbl: string = "Go";
  public unregisterBackButtonAction: any;
  public noSalonFound = false;
  public allSalon = [];
  public SalonHoursAll: SalonHour[];

  public salonsFetched = [];
  public allSalSerSubCat = [];
  public allSalonBackUp = [];
  public salonImagesURL = config.salonImgUrl;
  public techImagesURL = config.TechImageURL;
  public subStyleImagesBaseURL = config.StylesImagesURL;
  public defaultSalonPicName = "default.png";
  public showTagsPopOver = false;
  public myCustomer: Customer;
  // public salonCardOption: SALON_CARD_OPTIONS
  public sectionName = "";
  public SalonsServices: SalonServices[];
  // public shouldShowMiniHeader = false;
  public sal_id = 0;
  public distance = 0;
  public distanceUnit = "";
  // loading: any;
  public alertShown: boolean = false;
  public isServerSearching = false;
  constructor(
    public salonModal: SqliteDbProvider,
    private zone: NgZone,
    public loadingController: LoadingController,
    // public favModal: FavoriteModal,
    public events: Events,
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public globalSearch: GlobalServiceProvider,
    private viewCtrl: ViewController,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public keyboard: Keyboard,

    public salonServicesModal: SalonServicesModal,
    public customerModal: CustomerModal,
    private sqlite: SQLite,
    private ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public appCtrl: App,
    public geolocation: Geolocation,
    public alertCtrl: AlertController,
    public globalService: GlobalServiceProvider,
    public configModal: ConfigModal,
  ) // public favModel: FavoriteModal,
  {
    this.sectionName = this.navParams.get('sectionName')
    this.txtSearchSalon = "";
    this.distanceUnit = this.globalSearch.distance_unit;

  }

  loadSalons() {

    this.menu.swipeEnable(true);
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        !this.globalSearch.sty_id || this.globalSearch.sty_id.trim().length === 0 ? this.globalSearch.sty_id = "1" : ''
        this.salonModal.getAllSalonsByTypeFromDB(this.globalSearch.sty_id, this.sal_id, this.distance).then(myAllSalons => {
          this.allSalon = []
          this.allSalon = myAllSalons;
          !this.allSalon || this.allSalon.length == 0 ? this.noSalonFound = true : this.noSalonFound = false
          if (this.allSalon && this.allSalon.length > 0) {

            this.distance = this.allSalon[this.allSalon.length - 1].distance;
            this.sal_id = this.allSalon[this.allSalon.length - 1].sal_id;
          } else {
            this.allSalon = []
            !this.allSalon || this.allSalon.length == 0 ? this.noSalonFound = true : this.noSalonFound = false
            // alert('call here api to get salons against salon type ID')
          }

        });
      }

    });
  }
  wentToLoadMore = false;
  noMoreContentToLoad = false
  loadMoreSalons(infiniteScroll) {

    if (this.wentToLoadMore || this.sal_id === -1 || this.noMoreContentToLoad) {
      if (infiniteScroll) {
        try {
          infiniteScroll.complete();
        } catch (ex) { }
      }
      return;
    }
    console.log("LazyLoadingCalled");
    this.infiniteScroll = infiniteScroll;
    this.getPostFromDb();
  }
  getPostFromDb() {
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        // let serviceCat = this.navParams.get("fashionTrends");
        this.salonModal
          .getAllSalonsByTypeFromDB(
            this.globalSearch.sty_id,
            this.sal_id,
            this.distance
          )
          .then(
            res => {
              // console.log("MoreSalons", JSON.stringify(res));
              this.updatePageData(res);
            },
            error => { }
          );
      }
    });
  }
  updatePageData(res) {

    this.allSalon = this.allSalon.concat(res);
    this.infiniteScroll.complete();
    if (res && res.length > 0) {
      this.noSalonFound = false;
      if (this.allSalon && this.allSalon.length > 0) {
        this.distance = this.allSalon[this.allSalon.length - 1].distance;
        this.sal_id = this.allSalon[this.allSalon.length - 1].sal_id;
      }

      this.refreshSalonCard();
    } else {
      this.noSalonFound = true;
    }
    if (res && res.length < 8) {
      this.infiniteScroll.enable(false);
    }
    timer(4000).subscribe(() => {
      this.wentToLoadMore = false;
    });
  }
  public refreshSalonCard() {
    // this.salonCardOption = {
    //   CardHeight: '100vh',
    //   calledFrom: config.HomePage,
    //   ShouldShowSalonImage: true,
    //   shouldScroll: true,
    //   salon: this.allSalon,
    // }
  }
  public getSalDistance(val) {
    if (val.length > 3) {
      return val.substring(0, 3) + " miles";
    }
  }
  ionViewDidEnter() {

    // this.content.ionScrollEnd.subscribe((data) => {

    //   let dimensions = this.content.getContentDimensions();

    //   let scrollTop = this.content.scrollTop;
    //   let contentHeight = dimensions.contentHeight;
    //   let scrollHeight = dimensions.scrollHeight;
    //   // console.log('scrollTop ' + scrollTop + ' contentHeight ' + contentHeight + ' scrollHeight ' + scrollHeight);

    //   if ((scrollTop + contentHeight) > scrollHeight) {
    //     // alert('is bottom now')
    //     this.isScrolledToBottom = true
    //   } else {
    //     this.isScrolledToBottom = false
    //     // console.log(' or is bottom now')
    //   }

    // });
    this.getAndSaveAppConfig()
    this.initializeBackButtonCustomHandler();
    // this.events.subscribe("canShowMiniHeader", isHidden => {
    //   this.zone.run(() => {
    //     this.shouldShowMiniHeader = isHidden;
    //   });
    // });
  }
  ionViewDidLoad() {
    this.allOffers = [];
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getAllSalonOffers().then(
          offers => {
            this.allOffers = offers;
            this.showSalonDetail();
          },
          error => {
            this.showSalonDetail();
            console.log("Error In Getting Offerr");
          }
        );
      }
    });
  }
  showSalonDetail() {
    this.showTagsPopOver = false;
    let objPin = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_PIN
    );
    let objSubStyle = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_SUB_STYLE
    );

    if (objPin && objSubStyle) {
      this.pinImage = objPin.imageUrl;
      this.subStyleName = objSubStyle.ssc_name;
    }

    this.initializeBackButtonCustomHandler();

  }
  public latitude = '31.47203'
  public longitude = '74.38336'

  ionViewWillEnter() {

    this.getCustomer();
    let isFirstLaunch = this.serviceManager.getFromLocalStorage('__first_launch_for_home_page')

    if (!isFirstLaunch) {
      this.savePublicUser()
      this.getDoctorsFromServer(true)
    }
    else {

      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonModal.getAllSalonsFromSqliteDB().then(salons => {
            this.allSalon = []
            // this.allSalon = salons
            // this.allSalon.push(this.allSalon[0]) //= this.allSalon.concat(this.allSalon[0])
            // this.allSalon = this.allSalon.concat(this.allSalon)
            // this.allSalon = this.allSalon.concat(this.allSalon)
            // this.allSalon = this.allSalon.concat(this.allSalon)
            if (salons) {
              this.allSalon = salons
              this.getDoctorsFromServer(false)
            } else {
              this.getDoctorsFromServer(true)
            }
            // salons ? this.allSalon = salons : this.getDoctorsFromServer()
            if (this.allSalon.length <= 4) {
              if (this.infiniteScroll) this.infiniteScroll.enable(false);
              this.noMoreContentToLoad = true;
            } else {
              this.noMoreContentToLoad = true;
            }

          })
        }
      })

    }

  }
  getOfferBySalId(Id: string) {
    var found = this.allOffers.find(function (element) {
      return element.sal_id == Id;
    });
    return found;
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  shouldBeginEditing(value) { }
  endEndEditing(value) {

  }
  isSearchResponseBack = true;
  shouldChangeCharacter(search) {

    let searchValue: string = search.value;
    if (searchValue.trim().length === 0) {

      this.filteredTags = [];
      this.showTagsPopOver = false;
      this.txtSearchSalon = "";
      this.keyboard.close();
      return;
    }

    searchValue = searchValue.toLowerCase();
    console.log(searchValue);
    this.salonModal
      .searchSalonsByTypeFromDB(this.globalSearch.sty_id, searchValue)
      .then(searchedResult => {
        this.isSearchResponseBack = false;
        if (searchedResult && searchedResult.length > 0) {
          this.filteredTags = [];
          this.filteredTags = searchedResult;
          if (this.filteredTags && this.filteredTags.length > 0) {
            this.showTagsPopOver = true;
            console.log("showTagsPopOver " + this.showTagsPopOver);
          }
        } else {
          this.filteredTags = [];
          this.showTagsPopOver = false;
        }
      });
  }
  handleReturnkey() {
    this.keyboard.close();
  }

  getDoctorsFromServer(showSpinner) {
    if (!this.myCustomer) this.myCustomer = {}
    this.myCustomer.cust_id = '-1'
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("lst_salons"),
      cust_id: btoa(''),
      keywords: btoa(this.globalSearch.search_keyword),
      page_no: btoa("1"),
      sal_last_fetched: btoa(""),
      platform: btoa("ios"),
      app_version: btoa("5.0"),
      cust_zip: btoa(this.myCustomer.cust_zip),
      cust_lat: btoa(this.latitude.toString()),
      cust_lng: btoa(this.longitude.toString())
    };
    if (showSpinner) this.serviceManager.showProgress('Fetching Doctors, Please Wait....')
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })

      .subscribe(
        response => {

          this.serviceManager.setInLocalStorage('__first_launch_for_home_page', true)
          this.serviceManager.stopProgress()
          this.allSalon = []
          this.allSalon = response.salons
          this.allSalonBackUp = this.allSalon;

          this.allSalon.forEach(salon => {
            this.allSalonsIds += salon.sal_id + ','
          });

          this.allSalonsIds = this.allSalonsIds.slice(0, this.allSalonsIds.length - 1)

          this.calCulateSalHours(this.allSalonsIds);
          this.handel_lstSalonsService_Response(this.allSalon);
          this.allSalon && this.allSalon.length > 0 ? this.getAndSetSalonOffer(this.allSalon) : ''

          this.salonServicesModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.salonServicesModal.deleteMultipleSalonServiceCatogries(this.allSalonsIds).then(res => {
                this.salonServicesModal.saveSalonServicesCategoriesIntoDB(this.allSalon);
              })
              this.salonServicesModal.deleteMultipleSalonServices(this.allSalonsIds).then(res => {
                this.insertSalonServices(this.allSalonsIds);
              })
              this.salonServicesModal.saveSalonServicesIntoDB(this.allSalon)
            }
          })

          if (this.allSalon && this.allSalon.length > 0) {
            this.getAndSetSalonOffer(this.allSalon);
          }

          // this.salonServicesModal.getDatabaseState().subscribe(ready => {
          //   if (ready) {

          //     this.salonServicesModal.deleteMultipleSalonServiceCatogries(this.allSalonsIds).then(res => {
          //       this.salonServicesModal.saveSalonServicesCategoriesIntoDB(this.allSalon);
          //     })

          //     this.salonServicesModal.deleteMultipleSalonServices(this.allSalonsIds).then(res => {
          //       this.insertSalonServices(this.allSalon);
          //     })
          //   }
          // })
          this.salonModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.salonModal.deleteMultipleSalon(this.allSalonsIds).then(res => {
                if (this.allSalon != undefined && this.allSalon != null) {
                  this.salonModal.insert_salon_table(this.allSalon);
                }
              })
            }
          })

          let get_current_date_time = this.serviceManager.getCurrentDateTime();
          this.serviceManager.setInLocalStorage(this.serviceManager.LAST_FETCH_DATE_SALONS, get_current_date_time);
          this.serviceManager.setInLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER, this.allSalon)

          if (this.allSalon) {
            if (this.allSalon.length == 0) {
              this.noSalonFound = true;
            } else {
              this.noSalonFound = false;
            }
          } else {
            this.noSalonFound = true;
          }

          this.serviceManager.stopProgress

          this.serviceManager.setInLocalStorage(
            this.serviceManager.SALONS_NEAR_CUSTOMER,
            this.allSalon
          );

        },
        error => {
          this.serviceManager.stopProgress
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }
  ifPromotionMoreThan1(salId, salon) {
    this.offersForListing = [];

    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    if (this.offersForListing.length > 1) {
      return true;
    } else {
      return false;
    }
  }
  getPromotions(salId) {
    let promotions = this.getOfferBySalId(salId);
    if (promotions == null) {
      return "";
    } else {
      // console.log("Offer", JSON.stringify(promotions))
      return promotions.so_title;
    }
  }
  btnAllOfferTaped(salId, sal_name, sal_pic) {
    this.offersForListing = [];
    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    this.navCtrl.push(AllOffersPage, {
      offersForListing: this.offersForListing,
      sal_name: sal_name,
      sal_pic: sal_pic
    });

  }
  btnBackTapped() {

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }
  openDetailsPage(salon) {
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }

    this.nativePageTransitions.slide(this.options);

    this.navCtrl.push(SalonDetailsPage, {
      isServerSearch: false,
      selectedSalon: salon,
      offerType: offerType,
      sal_id: salon.sal_id
      // distance: salon.distance,
    });
  }
  openDetailsPageSearch(salon) {
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }
    this.nativePageTransitions.slide(this.options);

    // let eventAction = this.myCustomer.cust_name + 'Tapped on Salon ' + salon.sal_name + ' of id ' + salon.sal_id
    // this.ga.trackEvent('Tap', eventAction, 'lab: general label', 200, true)
    this.navCtrl.push(SalonDetailsPage, {
      isServerSearch: true,
      selectedSalon: salon,
      offerType: offerType,
      sal_id: salon.sal_id
    });
  }
  getSalonNameFirstCharacte(salonName: string): string {
    if (salonName.trim().length === 0) {
      return salonName;
    }
    var shortSalonName = "";
    let charCount = 0;
    let SplitSalonNames = salonName.split(" ");
    SplitSalonNames.forEach(element => {
      let salName: string = element;

      if (charCount < 4 && salName.trim().length > 0) {
        if (
          charCount === 3 &&
          salName.length === 1 &&
          SplitSalonNames.length > 3
        ) {
        } else {
          shortSalonName += salName.slice(0, 1);
          charCount += 1;
        }
      }
    });

    return shortSalonName.toLocaleUpperCase();
  }
  // public initializeBackButtonCustomHandler(): void {
  //   this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
  //     () => {
  //       this.customHandleBackButton();
  //     },
  //     10
  //   );
  // }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    }

    // this.nativePageTransitions.slide(this.options)
    // if (this.globalSearch.search_keyword.length != 0) {
    // this.navCtrl.push(LookingForPage, { isHideTab: "1" }, { animate: false })
    // } else {
    //   this.navCtrl.push(AppointmentsCardPage, { isHideTab: "1" }, { animate: false })
    // }
  }
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  localSearchArr = [];
  SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[] = [];
  SQL_TABLE_SEARCh = "search";
  filteredTags = [];
  public highlight(SubStyleName: string) {
    // console.log(SubStyleName);

    if (!this.txtSearchSalon) {
      return SubStyleName;
    }
    return SubStyleName.replace(
      new RegExp(this.txtSearchSalon, "gi"),
      match => {
        return '<span class="highlightText">' + match + "</span>";
      }
    );
  }
  btnStyleTapped(objSearch, index) {
    let objSubStyle: SALON_SERVICES_SEARCH = objSearch;

    if (!objSubStyle) {
      return;
    }

    this.showTagsPopOver = false;

    if (objSubStyle.keyword.length == 0) {
      this.serviceManager.makeToastOnFailure("Tag Name is missing");
      return;
    }

    this.txtSearchSalon = objSubStyle.keyword;

    let type = objSubStyle.type;
    if (type == "service") {
      console.log(objSubStyle);
      let sal_ids = "";
      let index = 0;
      let salonsServices = [];
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          // this.salonModal.getSalonServicesBySerId(objSubStyle.refer_id).then(res => {
          this.salonServicesModal.getSalonServicesBySerId(objSubStyle.refer_id).then(res => {
            salonsServices = res;
            console.log("AllSalonFromDb", salonsServices.length);
            salonsServices.forEach(element => {
              console.log("Block1", "Start");
              if (index == 0) {
                sal_ids = element.sal_id;
              } else {
                sal_ids = sal_ids + ", " + element.sal_id;
              }
              index = index + 1;
              console.log("SalId", element.sal_id + "SalId ");
            });

            console.log("Block1", "Completed");
            //block02 now we have all salId's get all salon from salon table against multiple sal_id's

            this.salonModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.salonModal
                  .getSalonsListingFromSqliteDB(sal_ids)
                  .then(res => {
                    this.allSalon = []
                    this.allSalon = res;

                    this.allSalonBackUp = this.allSalon;
                  });
              }
            });
            //here get all salon agains all salid's
          });
        }
      });
      //  this.showAllSalons(objSubStyle.refer_id);
    } else if (type == "salon") {
      this.salonServicesModal.getSalonServices;
      let sal_id = objSubStyle.refer_id;
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonModal.getSalonDetailsFrom(sal_id).then(mySalon => {
            if (mySalon) {
              console.log(JSON.stringify(mySalon));
              this.nativePageTransitions.slide(this.options);

              this.navCtrl.push(SalonDetailsPage, {
                selectedSalon: mySalon,
                isFromBookAppointment: true,
                sal_id: mySalon.sal_id
              });
            }
          });
        }
      });
    }
  }

  getCustomer() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              this.myCustomer = customer;
            }
          },
          error => {
            console.log("error: while getting customer");
          }
        );
      }
    });
  }

  //end favorite post section

  isScrolledToBottom = false;
  scrollChange = 0
  scrollHandler(event) {
    // return 
    console.log(event.scrollTop);

    if ((!this.allSalon && this.allSalon.length <= 4)) {
      return
    } else {

      let elem: HTMLElement = this.content._scrollContent.nativeElement

      if (event.directionY === 'down') {
        console.log('if .....');
        this.scrollChange = event.scrollTop
        if (event.scrollTop > 150) {
          elem.setAttribute("style", "margin-top: 0px !important;");
          this.zone.run(() => {
            this.slideOutNow = true;
          });
        }
      } else {
        this.zone.run(() => {
          // console.log('else .....');
          console.log('change: ' + (this.scrollChange - event.scrollTop).toString())
          let myChange = this.scrollChange - event.scrollTop
          if (myChange > 100) {

            elem.setAttribute("style", "margin-top: 130px !important;");
            this.zone.run(() => {
              this.slideOutNow = false
            });
          }

        });

      }

      this.keyboard.close()
      if (event.scrollTop <= 0) {
        elem.setAttribute("style", "margin-top: 130px !important;");
      }
    }

  }
  //search over server
  searchOverInternet() {
    if (this.txtSearchSalon.length > 2) {
      this.showTagsPopOver = false;
      this.isServerSearching = true;
      this.searchDataOverServer();

    }
  }
  public allSalonsIds = ''
  searchDataOverServer() {

    let loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait..."
    });
    loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("search_salons"),
      keywords: btoa(this.txtSearchSalon)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          loading.dismissAll();
          if (response.salons) {
            this.salonsFetched = response.salons;
          }

          this.handel_lstSalonsService_Response(this.salonsFetched);
          this.salonsFetched && this.salonsFetched.length > 0 ? this.getAndSetSalonOffer(this.salonsFetched) : ''
          // this.allSalonsIds = this.getSalonIds(this.salonsFetched);

          this.salonServicesModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.salonServicesModal.deleteMultipleSalonServiceCatogries(this.allSalonsIds).then(res => {

                this.salonServicesModal.saveSalonServicesCategoriesIntoDB(this.salonsFetched);
              })
              this.salonServicesModal.deleteMultipleSalonServices(this.allSalonsIds).then(res => {
                this.insertSalonServices(this.allSalonsIds);
              })
            }
          })
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }

  handel_lstSalonsService_Response(response) {

    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.deleteMultipleSalon(this.allSalonsIds).then(res => {
          if (
            this.salonsFetched != undefined &&
            this.salonsFetched != null
          ) {
            this.salonModal.insert_salon_table(this.salonsFetched);
          }
        });
      }
    });

  }
  calCulateSalHours(inactive_salons) {
    this.SalonHoursAll = [];
    this.allSalon.forEach(salon => {
      let sal_Id = salon.sal_id;
      let sal_hours = salon.sal_hours1;

      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(sal_hours);
      salonHours = sal_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = sal_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) +
            " - " +
            this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {

        let salonHoursObject = {
          sal_id: Number(sal_Id),
          sal_hours: salonHours[dayKey],
          sal_day: dayKey
        };

        this.SalonHoursAll.push(salonHoursObject);
      });
    });

    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        // if (inactive_salons) {
        //   this.salonModal.deleteSalHours(inactive_salons).then(isDataInserted => { });
        // }
        if (this.SalonHoursAll && this.SalonHoursAll.length > 0) {

          this.salonModal.saveIntoSalHoursTable(this.SalonHoursAll).then(isDataInserted => {
            console.log("InsertedAllSalHours", isDataInserted);
          });
        }
      }
    });
  }
  convert24Hrto12Hr(time) {
    if (time === undefined || time === null) {
      this.navCtrl.pop();
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {

      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    time[0] = this.padToTwo(time[0]);
    return time.join(""); // return adjusted time or original string
  }
  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }
  getAndSetSalonOffer(salons) {
    let so_ids = "";
    this.ALLOFFERS = [];
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          // so_ids += item['so_id'] + ','
          this.ALLOFFERS.push(item);
        }
      }
    });
    // so_ids = so_ids.slice(0, so_ids.length - 1)//after loop
    this.saveSalonOffers(this.ALLOFFERS);
  }
  saveSalonOffers(ALLOFFERS) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (ALLOFFERS != undefined && ALLOFFERS != null) {
          this.salonServicesModal.insert_salon_offer(ALLOFFERS);
        }
      }
    });
  }
  insertSalonServices(salonIds) {
    if (!this.salonsFetched) { return; }
    this.SalonsServices = [];
    this.salonsFetched.forEach(salon => {
      this.SalonsServices = this.SalonsServices.concat(salon.services);
    });
    if (this.SalonsServices && this.SalonsServices.length > 0) {
      this.serviceManager.setInLocalStorage(
        this.serviceManager.NEAREST_SALON_SERVICES,
        this.SalonsServices
      );
    }

  }
  getSalonIds(response) {
    let salonIds = "";
    let i = 0;
    response.forEach(element => {
      salonIds += element.sal_id + ",";
    });
    salonIds = salonIds.slice(0, salonIds.length - 1);
    return salonIds;
  }
  roundRatingUpToOneDecimal(ratingValue) {
    return Math.round(ratingValue).toFixed(1);
  }

  public deviceType = ''
  // zahidd
  savePublicUser() {

    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);

    if (this.platform.is('ios')) {
      this.deviceType = "1";
    }
    if (this.platform.is('android')) {
      this.deviceType = "2";
    }
    const params = {
      service: btoa('save_public_user'),
      pu_device_id: btoa(token),
      pu_device_type: btoa(this.deviceType),
      pu_lat: btoa(this.latitude.toString()),
      pu_lng: btoa(this.longitude.toString()),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          let publicUserID = 0
          publicUserID = res.pu_id
          !publicUserID ? publicUserID = 0 : ''

          this.myCustomer = {};
          this.myCustomer.cust_id = publicUserID.toString()
          this.serviceManager.setInLocalStorage(this.serviceManager.PUBLIC_USER_ID, res.pu_id)

        },
        (error) => {

          console.log(error);

          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        },
        () => {
          // this.serviceManager.stopProgress()
        }
      )
  }

  getSalonIdsAndInsertSalonsToSearchTable(response, inactive_salons_ids) {
    !inactive_salons_ids ? inactive_salons_ids = '' : ''

    this.SALON_SERVICE_SEARCH = []
    let salonIds = '';
    let i = 0;
    response.forEach(element => {
      //for search table 
      let item = {
        keyword: element.sal_name,
        type: "salon",
        refer_id: element.sal_id,
        image_url: element.sal_pic
      }
      this.SALON_SERVICE_SEARCH.push(item)
      salonIds += element.sal_id + ','
    });
    salonIds = salonIds.slice(0, salonIds.length - 1)
    if (salonIds.length > 0) {
      salonIds = salonIds + "," + inactive_salons_ids;
    } else {
      salonIds = inactive_salons_ids;
    }

    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        if (salonIds !== undefined && salonIds !== null) {
          this.salonModal.deleteSearchKeyWordsBySalId(salonIds, 'salon').then(isDataInserted => {
          })
        }
        if (this.SALON_SERVICE_SEARCH !== undefined && this.SALON_SERVICE_SEARCH !== null && this.SALON_SERVICE_SEARCH.length > 0) {
          this.salonModal.InsertInTblSearch(this.SALON_SERVICE_SEARCH).then(isDataInserted => {
          })
        }
      }
    })
    // this.salonModal.insert_saerch_table_new(this.SALON_SERVICE_SEARCH);
    return salonIds;
  }
  alert
  presentConfirm() {
    this.alert = this.alertCtrl.create({
      title: 'Confirm exit....',
      message: 'Do you want to exit app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            // this.customHandleBackButton();
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert.dis
    this.alert.present().then(() => {
      this.alertShown = true;
    });

  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {

      if (this.menu.isOpen()) {
        this.menu.close();
      } else {
        if (this.alertShown == false) {
          this.alertShown = true
          this.presentConfirm();
        } else {
          this.alertShown = false
          this.alert.dismiss()
        }
      }

    }, 10);
  }
  getAndSaveAppConfig() {

    let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')

    if (_appConfig && _appConfig.currency) {
      this.globalService.currencySymbol = _appConfig.currency
      this.globalService.distance_unit = _appConfig.distance_unit

    } else {
      this.configModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.configModal.getAllAppConfig().then(data => {

            if (data && data.currency) {
              this.globalService.currencySymbol = data.currency
              this.globalService.distance_unit = data.distance_unit

            } else {
              this.getAppConfigFromServer()

            }

          })
        }
      })
    }

    console.log('eenenenene');

  }
  getAppConfigFromServer() {

    const params = {
      service: btoa('get_config'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          let apConfigData: APP_CONFIG = res.config

          this.serviceManager.setInLocalStorage('isAppConfigs', apConfigData)

          if (apConfigData) {
            this.globalService.currencySymbol = apConfigData.currency
            this.globalService.distance_unit = apConfigData.distance_unit
          }
          this.configModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.configModal.InsertInAppConfigTable(apConfigData)
            }
          })
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
}
