import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
// import{MainHomePage} from '../main-home/main-home';
import { EmailComposer } from '@ionic-native/email-composer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Menu } from 'ionic-angular/components/app/menu-interface';
import { FormControl } from '@angular/forms';
import { GlobalProvider } from './../../providers/global/global';
import { HomePage } from '../home/home';
//https://ionicacademy.com/emails-image-attachments-ionic/
/**
 * //https://www.djamware.com/post/58a1378480aca7386754130a/ionic-2-fcm-push-notification
 */
@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {
  currentImage = null;
  public privacy_policy_test=''
  public unregisterBackButtonAction: any;
  gcmTocken:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private emailComposer: EmailComposer, private camera: Camera,
    private viewCtrl: ViewController,
    public  platform:Platform,
    public serviceManager: GlobalProvider,
    private menu : MenuController,
    private loadingController: LoadingController,
    private alertCtrl: AlertController
  ) {
  }
  ionViewDidLoad() {
  }  
  goBack(){
    this.navCtrl.setRoot(HomePage);
  }
ionViewWillEnter(){
  this.getPrivacyPolicy();
  this.menu.swipeEnable(false);
}
  //custom back button
ionViewDidEnter() {
  this.initializeBackButtonCustomHandler();
}
ionViewWillLeave() {
  this.unregisterBackButtonAction && this.unregisterBackButtonAction();
}
public initializeBackButtonCustomHandler(): void {
  this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
  }, 10);
}
private customHandleBackButton(): void {
  // this.navCtrl.setRoot(HomePage);
  if(this.menu.isOpen()){
    this.menu.close();
  }else {
     if (this.navCtrl.canGoBack()) {
    this.navCtrl.pop();
  } else {
    this.navCtrl.setRoot(HomePage)
  }
  }

}
getPrivacyPolicy() {
  let loading = this.loadingController.create({ content: "Please Wait..." });
  loading.present();
  var params = {
    service: btoa("get_config")
  }
  console.log('params',JSON.stringify(params))
  this.serviceManager.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
        retryCount += 1;
        if (retryCount < 3) {
          return retryCount;
        }
        else {
          throw (err);
        }
      }, 0).delay(1000)
    })
    .subscribe((response) => {
      loading.dismiss()
      // alert('response')
    //  this.saveHomePageCounts(response)
    if(response.config){
      this.privacy_policy_test=response.config.privacy_policy;
   }
    console.log('privacy_policy_test',this.privacy_policy_test)
      // console.log('Response',JSON.stringify(response))
    },
      error => {
        loading.dismissAll();
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
      });
}
presentPrompt() {
  let alert = this.alertCtrl.create({
    title: 'Login',
    inputs: [
      {
        name: 'username',
        placeholder: 'Username'
      },
      {
        name: 'password',
        placeholder: 'Password',
        type: 'password'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Login',
        handler: data => {
          // if (User.isValid(data.username, data.password)) {
          //   // logged in!
          // } else {
          //   // invalid login
          //   return false;
          // }
        }
      }
    ]
  });

  alert.present();
}
}