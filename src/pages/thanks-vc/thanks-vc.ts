import { MainHomePage } from './../main-home/main-home';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Navbar } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { AppointmentsPage } from '../appointments/appointments';
import { MenuController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-thanks-vc',
  templateUrl: 'thanks-vc.html',
})
export class ThanksVcPage {
  @ViewChild(Navbar) navBar: Navbar;

  public unregisterBackButtonAction: any;
  salonName: string
  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public global: GlobalServiceProvider,
    private nativePageTransitions: NativePageTransitions,
  ) {
    this.salonName = this.navParams.get('sal_name');
  }

  //custom back button
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.setRoot(HomePage);
    if(this.menu.isOpen()){
      this.menu.close();
    }
  }
  //end custom back button

  ionViewDidLoad() {
    // this.navBar.backButtonClick = (e: UIEvent) => {

    //   let options: NativeTransitionOptions = {
    //     direction: 'right',
    //     duration: 500,
    //     slowdownfactor: 3,
    //     slidePixels: 20,
    //     iosdelay: 100,
    //     androiddelay: 150,
    //     fixedPixelsTop: 0,
    //     fixedPixelsBottom: 0
    //   };
    //   if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    //   if (this.navCtrl.canGoBack()) {
    //     this.navCtrl.pop({
    //       animate: false,
    //       animation: 'ios-transition',
    //       direction: 'back',
    //       duration : 500,
    //     })
    //   } else {
    //     this.navCtrl.setRoot(HomePage)
    //   }
    // }
  }
  goToAppointments() {
    let transitionOptions: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.navCtrl.setRoot(AppointmentsPage, {opentab: 1})
    //below code before hiding tab-bar
    
    // let isHomePageInStack = false
    // for (let i = 0; i < this.navCtrl.length(); i++) {
    //   let v = this.navCtrl.getViews()[i];
    //   console.log(v.component.name);
    //   if (!isHomePageInStack && v.component.name === 'AppointmentsPage' ||v.component.name === 'HomePage') {
    //     isHomePageInStack = true
    //   }
    // }
    // console.log(this.navCtrl.getViews()[0].component.name)
    // if (isHomePageInStack) {
    //   this.navCtrl.popToRoot();
    //   this.nativePageTransitions.slide(transitionOptions)
    //   this.navCtrl.parent.select(1);
      
    // } else {
    //   // When appointment is made from latest Trends
    //   this.nativePageTransitions.slide(transitionOptions)
    //   this.navCtrl.popToRoot();
    //   this.navCtrl.setRoot(TabsPage, {opentab: 1})
    // }

  }

}
